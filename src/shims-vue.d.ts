declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "vuetify" {
  import Vuetify from "vuetify/types";
  export default Vuetify;
}
