import { expect } from "chai";
import { shallowMount, Wrapper } from "@vue/test-utils";
import Form from "@/components/Form.vue";

describe("HelloWorld.vue", () => {
  let wrapper: Wrapper<Vue>;
  before(() => {
    wrapper = shallowMount(Form, {
      propsData: {
        value: {
          foo: "bar"
        },
        fields: [
          {
            id: "text",
            type: "text",
            options: {
              required: true,
              counter: true,
              outline: true,
              label: "text"
            }
          }
        ]
      }
    });
  });
  it("is a Vue instance", () => {
    expect(wrapper.isVueInstance());
  });
});
