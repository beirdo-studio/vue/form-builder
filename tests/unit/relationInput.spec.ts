import RelationInput from "@/components/Fields/RelationInput.vue";
import { shallowMount, Wrapper } from "@vue/test-utils";
import { expect } from "chai";
import sinon from "sinon";

describe("RelationInput", () => {
  let wrapper: Wrapper<Vue>;

  before(() => {
    wrapper = shallowMount(RelationInput);
  });
  it("", async () => {
    const getItemsStub = sinon
      .stub()
      .resolves([{ value: "test", text: "test" }]);
    wrapper.setMethods({ getItems: getItemsStub });

    const input = wrapper.findComponent({ name: "v-autocomplete" });
    expect(wrapper.vm.$data.items).to.be.deep.equal([]);
    await input.vm.$emit("focus");
    expect(wrapper.vm.$data.items).to.be.deep.equal([
      { value: "test", text: "test" }
    ]);
    await input.vm.$emit("blur");
    await input.vm.$emit("focus");
    expect(wrapper.vm.$data.items).to.be.deep.equal([
      { value: "test", text: "test" }
    ]);
  });
});
