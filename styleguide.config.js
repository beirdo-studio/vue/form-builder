import path from "path";

module.exports = {
  // set your styleguidist configuration here
  require: [path.join(__dirname, "styleguide/global.requires.js")],

  title: "Form Builder",
  components: "src/components/**/[A-Z]*.vue",

  // sections: [
  //   {
  //     name: 'First Section',
  //     components: 'src/components/**/[A-Z]*.vue'
  //   }
  // ],
  // webpackConfig: {
  //   // custom config goes here
  // }
  defaultExample: false,
  pagePerSection: true,
  usageMode: "expand",
  exampleMode: "expand",
  skipComponentsWithoutExample: false
};
