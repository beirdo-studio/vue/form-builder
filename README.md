# Form Builder

## Project setup
```
yarn install
```

### Compile and hot-reload for development
```
yarn run serve
```

### Compile and minify for production
```
yarn run build
```

### Run tests
```
yarn run test
```

### Lint and fixes files
```
yarn run lint
```

### Run end-to-end tests
```
yarn run test:e2e
```

### Run unit tests
```
yarn run test:unit
```

### Run styleguide server
```
yarn run styleguide
```

### Compile and minify styleguide for production 
```
yarn run styleguide:build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

 
